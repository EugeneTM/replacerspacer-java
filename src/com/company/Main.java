package com.company;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.Parent;

import static com.sun.org.apache.xalan.internal.utils.SecuritySupport.getResourceAsStream;

public class Main extends Application{

    public static void main(String[] args) {launch(args);}

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("ReplacerSpacer");

        primaryStage.getIcons().add(new Image("/assets/file_replace_icon_136634.png"));
        primaryStage.setMaxWidth(815);
        primaryStage.setMaxHeight(325);

        primaryStage.setMinWidth(815);
        primaryStage.setMinHeight(325);

        Scene scene = new Scene(parent, 800, 287);
        scene.getStylesheets().add("/css/style.css");
        primaryStage.setScene(scene);
        //"/src/css/style.css"
        primaryStage.show();
    }
}
