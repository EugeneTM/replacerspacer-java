package com.company;

import com.sun.istack.internal.Nullable;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class Shake {
    private TranslateTransition translateTransition;
    private TranslateTransition translateTransition2;

    public Shake(Node node) {
        translateTransition = new TranslateTransition(Duration.millis(70), node);
        translateTransition.setFromX(-10f);
        translateTransition.setByX(10f);
        translateTransition.setCycleCount(3);
        translateTransition.setAutoReverse(true);
    }
    public Shake(){

    }

    public void PlayAnimate(){
        translateTransition.playFromStart();
    }

    public void RaceText(Node node) {
        translateTransition2 = new TranslateTransition(Duration.millis(7), node);
        translateTransition2.setFromX(800f);
        translateTransition2.setByX(-800f);
        translateTransition2.setCycleCount(1);
        translateTransition2.setAutoReverse(true);
    }

    public void PlayAnimateRaceText(Text text){
        translateTransition2.playFromStart();
//        FadeText(text, 1);
    }

    public void FadeText(Text msg, int duration){
        /* Set up a Timeline animation */
        // Create the initial and final key frames
        KeyValue initKeyValue = new KeyValue(msg.opacityProperty(), 0.0);
        KeyFrame initFrame = new KeyFrame(Duration.ZERO, initKeyValue);
        KeyValue endKeyValue = new KeyValue(msg.opacityProperty(), 15.0);
        KeyFrame endFrame = new KeyFrame(Duration.seconds(duration), endKeyValue);
        // Create a Timeline object
        Timeline timeline = new Timeline(initFrame, endFrame);
        // Let the animation run forever
        timeline.setCycleCount(1);
        // Start the animation
        timeline.play();
    }
}
