package com.company;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.net.URL;
import java.util.ResourceBundle;

import com.etlib.etAnimated.IAnimatedEffects;
import com.etlib.etAnimated.MovingText;
import com.etlib.etAnimated.etShakeEffects;
import com.etlib.etClipboard.IClipboardBuffer;
import com.etlib.etClipboard.etClipboardBuffer;
import com.etlib.etDialog.IEtDialog;
import com.etlib.etDialog.etDialog;
import com.etlib.etReplace.IReplacer;
import com.etlib.etReplace.Replacer;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Controller {

    private String resultTextReplacing;
    private final IAnimatedEffects iAnimatedEffects;
    private final IClipboardBuffer iClipboardBuffer;
    private final IEtDialog iEtDialog;
    private final IReplacer iReplacer;

    public Controller() {
        iAnimatedEffects = new etShakeEffects();
        iClipboardBuffer = new etClipboardBuffer();
        iEtDialog = new etDialog();
        iReplacer = new Replacer();
    }

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="clipboardInfo"
    private TextField clipboardInfo; // Value injected by FXMLLoader

    @FXML // fx:id="resultReplacement"
    private TextArea resultReplacement; // Value injected by FXMLLoader

    @FXML // fx:id="targetText"
    private TextField targetText; // Value injected by FXMLLoader

    @FXML // fx:id="fromText"
    private TextField fromText; // Value injected by FXMLLoader

    @FXML // fx:id="toText"
    private TextField toText; // Value injected by FXMLLoader

    @FXML // fx:id="buttonReplace"
    private Button buttonReplace; // Value injected by FXMLLoader

    @FXML // fx:id="buttonClipboard"
    private Button buttonClipboard; // Value injected by FXMLLoader

    @FXML // fx:id="buttonClear"
    private Button buttonClear; // Value injected by FXMLLoader

    @FXML // fx:id="textReplacerSpacer"
    private Text textReplacerSpacer; // Value injected by FXMLLoader

    @FXML // fx:id="line"
    private Line line; // Value injected by FXMLLoader

    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        initializeField();
        initializeAnimated();
        initializeReplace();
        initializeClipboard();
        initializeClear();
        initializeStyle();
    }

    private void initializeStyle() {
        buttonReplace.getStyleClass().add("button");
        buttonClear.getStyleClass().add("buttonClear");
        buttonClipboard.getStyleClass().add("buttonClipboard");
        clipboardInfo.getStyleClass().add("clipboardInfo");
    }

    private void initializeAnimated() {
//        iAnimatedEffects.MovingText(textReplacerSpacer, 10, 300, -300f, null, true, true);
        iAnimatedEffects.FadeText(textReplacerSpacer, 50, 0.0f, 5.0f, 1, false);
    }

    private void initializeClear() {
        buttonClear.setOnAction(event -> {
            Clear();
            iAnimatedEffects.PlayAnimate(textReplacerSpacer);
        });
    }

    private void initializeClipboard() {
        buttonClipboard.setOnAction(event -> {
            String result = iClipboardBuffer.copyFromBufferClipboard();
            targetText.setText(result);
            resultReplacement.clear();
            clipboardInfo.setText(result);
            iAnimatedEffects.PlayAnimate(textReplacerSpacer);
        });
    }

    private void initializeReplace() {
        buttonReplace.setOnAction(event -> {

            if (targetText.getText().isEmpty()) {
                iAnimatedEffects.Shake(buttonReplace);
                iAnimatedEffects.PlayAnimate();
                iEtDialog.CreateDialogError("Ошибка", "Строка для замены отсутствует.", Alert.AlertType.INFORMATION);
            }

            if (fromText.getText().isEmpty() && fromText.getText().contains(" ")) {
                iAnimatedEffects.Shake(buttonReplace);
                iAnimatedEffects.PlayAnimate();
                iEtDialog.CreateDialogError("Ошибка", "Символ ["+ fromText.getText() +"] который нужно заменить, отсутствуют. Значение символов не может быть пустым.", Alert.AlertType.INFORMATION);
            }

            for (int i = 0; i < fromText.getText().length(); i++) {
                if (targetText.getText().indexOf(fromText.getText().toCharArray()[i]) == -1 && fromText.getText() != " ") {
                    iAnimatedEffects.Shake(buttonReplace);
                    iAnimatedEffects.PlayAnimate();
                    iEtDialog.CreateDialogError("Ошибка", "В строке отсутствует символ для замены. [" + (fromText.getText() == " " ? "Символ пробела" : fromText.getText()) + "]", Alert.AlertType.INFORMATION);
                }
            }

            resultTextReplacing = iReplacer.Replacer(fromText.getText(), toText.getText(), targetText.getText());

            OutputResultOnTextArey();

            iClipboardBuffer.copyFromBufferClipboard();
            iClipboardBuffer.pasteToBufferClipboard(resultTextReplacing);

            clipboardInfo.setText("Результат скопирован в буффер обмена.");
            targetText.clear();
            fromText.clear();
            toText.setText("_");

//            iAnimatedEffects.PlayAnimate(textReplacerSpacer);
        });
    }

    void initializeField() {
        buttonReplace.defaultButtonProperty().bind(buttonReplace.focusedProperty()); // Если не срабатывает Enter
        buttonClear.defaultButtonProperty().bind(buttonClear.focusedProperty()); // Если не срабатывает Enter
        buttonClipboard.defaultButtonProperty().bind(buttonClipboard.focusedProperty()); // Если не срабатывает Enter
        toText.setText("_");
        textReplacerSpacer.setText("ReplacerSpacer v1.0");
        textReplacerSpacer.setFont(new Font("Leelawadee", 20));
        targetText.setText(iClipboardBuffer.copyFromBufferClipboard());
    }

    public void Clear() {
        fromText.clear();
        toText.clear();
        targetText.clear();
        resultReplacement.clear();
        String clip = iClipboardBuffer.copyFromBufferClipboard();

        if (clip.isEmpty()) {
            clipboardInfo.setText("");
        } else {
            clipboardInfo.setText(clip);
        }

        toText.setText("_");
    }
    public void OutputResultOnTextArey() {
        resultReplacement.setText(resultTextReplacing);
    }
}

